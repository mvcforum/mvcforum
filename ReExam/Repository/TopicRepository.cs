﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReExam.Models.Topics;
using ReExam.Models;
using System.Data.Entity;

namespace ReExam.Repository
{
    public class TopicRepository : IRepositoryTopic
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public void Delete(int id)
        {
            Topic topic = Find(id);
            if (topic != null)
            {
                db.Topics.Remove(topic);

            }

        }

        public Topic Find(int id)
        {
            Topic topic = db.Topics.Find(id);
            return topic;
        }

        public IEnumerable<Topic> GetAll()
        {

            return db.Topics.ToList();
        }


        public IEnumerable<Comment> GetComments(int id)
        {
            var list =
                 from comment in db.Comments
                 where comment.PostId.Equals(id)
                 orderby comment.CommentId descending
                 select comment;
            return list;
        }





        public IEnumerable<Post> GetPosts(int id)
        {
            var list =
                from posts in db.Posts
                where posts.TopicId.Equals(id)
                orderby posts.PostId descending
                select posts;
            return list;
        }




        public void InsertOrUpdate(Topic topic)
        {
            if (topic.TopicId == 0)
            {
                db.Topics.Add(topic);
            }
            else
            {
                db.Entry(topic).State = EntityState.Modified;
            }
            db.SaveChanges();
        }

        public void InsertOrUpdatePost(Comment comment)
        {
            if (comment.CommentId== 0)
            {
                db.Comments.Add(comment);
            }
            else
            {
                db.Entry(comment).State = EntityState.Modified;
            }
            db.SaveChanges();
        }

        public void InsertOrUpdatePost(Post post)
        {
            if (post.PostId == 0)
            {
                db.Posts.Add(post);
            }
            else
            {
                db.Entry(post).State = EntityState.Modified;
            }
            db.SaveChanges();
        }
    }
}