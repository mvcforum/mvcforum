﻿using ReExam.Models;
using ReExam.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ReExam.Repository
{
    public class UserRepository: IRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public void Delete(int id)
        {
            UserModel user = db.Useres.Find(id);
            db.Useres.Remove(user);
            db.SaveChanges();

        }

        public UserModel Find(int id)
        {
            UserModel user = db.Useres.Find(id);
            return user;    

        }

        public IEnumerable<UserModel> GetAll()
        {
            return db.Useres.ToList();
        }

        public void InsertOrUpdate(UserModel user)
        {
            if (user.UserModelID == 0)
            {
                db.Useres.Add(user);
            }
            else
            {
                db.Entry(user).State = EntityState.Modified;
            }
            db.SaveChanges();
        }
    }
}