﻿using ReExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReExam.Repository
{
    interface IRepository
    {
        IEnumerable<UserModel> GetAll();
        UserModel Find(int id);
        void Delete(int id);
        void InsertOrUpdate(UserModel User);
    }
}
