﻿using ReExam.Models;
using ReExam.Models.Topics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReExam.Repository
{
    interface IRepositoryTopic
    {
        IEnumerable<Topic> GetAll();
        IEnumerable<Post> GetPosts(int id);
        IEnumerable<Comment> GetComments(int id);
        Topic Find(int id);
        void Delete(int id);
        void InsertOrUpdate(Topic topic);
        void InsertOrUpdatePost(Post post);
        void InsertOrUpdatePost(Comment comment);
    }
}
