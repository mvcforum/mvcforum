﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReExam.Startup))]
namespace ReExam
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
