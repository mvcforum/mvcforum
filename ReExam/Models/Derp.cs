﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReExam.Models
{
    public class Derp
    {
        public int DerpId { get; set; }
        public String Name { get; set; }
        public int MyProperty { get; set; }
        public int Herp { get; set; }
    }
}