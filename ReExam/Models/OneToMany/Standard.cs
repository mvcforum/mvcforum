﻿using ReExam.Models.OneToMany;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReExam.Models
{
    public class Standard
    {

        public int StandardId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Student> Students { get; set; }    }
}