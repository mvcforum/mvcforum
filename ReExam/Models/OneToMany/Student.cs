﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ReExam.Models.OneToMany
{
    public class Student
    {
        public Student() {}
    public int StudentId { get; set; }
    public string StudentName { get; set; }


        // StandardId is FKey   
    public int StandardId { get; set; }
    public virtual Standard Standard { get; set; }
}
}