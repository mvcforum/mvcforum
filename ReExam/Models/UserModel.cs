﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReExam.Models
{
    public class UserModel
    {
        public int UserModelID { get; set; }
        public String Email { get; set; }
        public String PassWord { get; set; }
        public String UserName { get; set; }
    }
}