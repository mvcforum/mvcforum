﻿using ReExam.Models.Topics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReExam.Models
{
    public class Comment
    {
        public int CommentId { get; set; }
        public String CommentText { get; set; }

        public int  PostId { get; set; }
        public virtual Post post { get; set; }
    }
}