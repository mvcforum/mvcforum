﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReExam.Models.Topics
{
    public class Post
    {
        public int PostId { get; set; }
        public String PostName { get; set; }
        public String OriginalPost { get; set; }

        public ICollection<Comment> Comments { get; set; }

        public int TopicId { get; set; }
        public virtual Topic Topic{ get; set; }

    }
}