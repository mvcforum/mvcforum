﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReExam.Models.Topics
{
    public class Topic
    {
        public int TopicId { get; set; }
        public String TopicName { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

    }
}