﻿using ReExam.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReExam.Controllers
{
    public class UserController : Controller
    {
        UserRepository userRepo = new UserRepository();
        // GET: User
        public ActionResult Index()
        {
            return View(userRepo.GetAll());
        }
    }
}