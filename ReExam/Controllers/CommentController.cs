﻿using ReExam.Models;
using ReExam.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReExam.Controllers
{
    public class CommentController : Controller
    {
        TopicRepository commentRepo = new TopicRepository();
        // GET: Comment
        public ActionResult Index(int id)
        {
            return View(commentRepo.GetComments(id));
        }
        [HttpGet]
        public ActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Create(Comment comment)
        {
            if (ModelState.IsValid)
            {
                commentRepo.InsertOrUpdatePost(comment);
                return RedirectToAction("Index");
            }
            return View();

        }


    }
}