﻿using ReExam.Repository;
using ReExam.Models.Topics;
using System.Web.Mvc;

namespace ReExam.Controllers
{
    public class PostController : Controller
    {
        TopicRepository PostRepo = new TopicRepository();

        // GET: Post
        // ToDo fix error, from html that action link.
        public ActionResult Index(int id)
        {
            return View(PostRepo.GetPosts(id));
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpGet]
        public ActionResult SelectPost(int id)
        {
            return RedirectToAction("Index", "Comment", id);
        }
        [HttpPost]
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                PostRepo.InsertOrUpdatePost(post);
                return RedirectToAction("Index");
            }
            return View();

        }
    }
}