﻿using ReExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReExam.Controllers
{
    public class DerpController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
 
        // GET: Derp
        public ActionResult Index()
        {

          return View(db.Derps.ToList());
        }
    }
}