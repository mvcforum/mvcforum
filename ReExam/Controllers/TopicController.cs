﻿using ReExam.Models.Topics;
using ReExam.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReExam.Controllers
{
    public class TopicController : Controller
    {
        TopicRepository topicRepo = new TopicRepository();
        // GET: Topic
        public ActionResult Index()
        {
            return View(topicRepo.GetAll());
        }
        [HttpGet]
        //[Authorize(Roles ="PowerUser, Admin")]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Topic topic)
        {
            if (ModelState.IsValid)
            {
                topicRepo.InsertOrUpdate(topic);
                return RedirectToAction("Index");
            }
            return View();
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
           return View(topicRepo.Find(id));
         }

        [HttpPost]
        public ActionResult Delete( Topic topic)
        {
            topicRepo.Delete(topic.TopicId);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            return View(topicRepo.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(Topic topic)
        {
            if(ModelState.IsValid)
            {
                topicRepo.InsertOrUpdate(topic);
            }
           
            return View();
        }
        /*
        public ActionResult SelectTopic(int id)
        {
          return  RedirectToAction("Index", "Post", id);
        }
        */
    }
}