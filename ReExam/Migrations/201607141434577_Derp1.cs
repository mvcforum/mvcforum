namespace ReExam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Derp1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Derps", "MyProperty", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Derps", "MyProperty");
        }
    }
}
