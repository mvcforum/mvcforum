namespace ReExam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostModelWOP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "OriginalPost", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "OriginalPost");
        }
    }
}
