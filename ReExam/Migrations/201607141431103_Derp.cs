namespace ReExam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Derp : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Derps",
                c => new
                    {
                        DerpId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.DerpId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Derps");
        }
    }
}
